﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RetrioService.Api.Entities;

namespace RetrioService.Api.Services
{
    public interface IGroupService
    {
        Task<List<Group>> GetByCategoryId(int id);
    }

    public class GroupService : IGroupService
    {
        private readonly RetrioContext _dbContext;

        public GroupService(RetrioContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<Group>> GetByCategoryId(int id)
        {
            var groupIds = _dbContext.Cards.Where(c => c.CategoryId == id).Select(c => c.GroupId);
            var groups = new List<Group>();
            foreach (var groupId in groupIds)
            {
                groups.Add(await _dbContext.Groups.FirstAsync(g => g.GroupId == groupId));
            }
            return groups;
        }
    }
}