﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RetrioService.Api.Entities;

namespace RetrioService.Api.Services
{
    public interface IUserService
    {
        Task<User> GetUser(Guid guid);
        Task CreateUser(User user);
    }

    public class UserService : IUserService
    {
        private readonly RetrioContext _dbContext;

        public UserService(RetrioContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task<User> GetUser(Guid guid)
        {
            return _dbContext.Users.FirstOrDefaultAsync(u => u.UserId == guid);
        }

        public async Task CreateUser(User user)
        {
            _dbContext.Users.Add(user);
            await _dbContext.SaveChangesAsync();
        }
    }

    
}