﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RetrioService.Api.Entities;

namespace RetrioService.Api.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly RetrioContext _dbContext;

        public CategoryService(RetrioContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task<Category> GetCategory(int id)
        {
            return _dbContext.Categories.FirstOrDefaultAsync(c => c.CategoryId == id);
        }

        public List<Category> GetCategoriesBySession(Guid guid)
        {
            return _dbContext.Categories.Where(c => c.SessionId == guid).ToList();
        }

        public async Task<Category> CreateCategory(Category category)
        {
            _dbContext.Categories.Add(category);
            await _dbContext.SaveChangesAsync();
            return category;
        }

        public async Task UpdateCategory(Category category)
        {
            _dbContext.Categories.Update(category);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteCategory(int id)
        {
            var category = await _dbContext.Categories.FirstOrDefaultAsync(c => c.CategoryId == id);
            if (category != null)
            {
                _dbContext.Categories.Remove(category);
                await _dbContext.SaveChangesAsync();
            }
        }
    }

    public interface ICategoryService
    {
        Task<Category> GetCategory(int id);
        List<Category> GetCategoriesBySession(Guid session);
        Task<Category> CreateCategory(Category category);
        Task UpdateCategory(Category category);
        Task DeleteCategory(int id);
    }
}