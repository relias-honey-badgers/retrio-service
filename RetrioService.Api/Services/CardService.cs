﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RetrioService.Api.Entities;

namespace RetrioService.Api.Services
{
    public class CardService : ICardService
    {
        private readonly RetrioContext _dbContext;

        public CardService(RetrioContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Card> AddCard(Card card)
        {
            _dbContext.Cards.Add(card);
            await _dbContext.SaveChangesAsync();
            return card;
        }

        public List<Card> GetCardsByCategory(int categoryId)
        {
            return _dbContext.Cards.Where(i => i.CategoryId == categoryId).ToList();
        }

        public List<Card> GetCardsInGroup(int groupId)
        {
            return _dbContext.Cards.Where(c => c.GroupId == groupId).ToList();
        }

        public async Task Delete(Card card)
        {
            var cardToDelete = await _dbContext.Cards.AsNoTracking().FirstOrDefaultAsync(c => c.CardId == card.CardId);
            if (cardToDelete != null)
            {
                _dbContext.Cards.Remove(card);
                await _dbContext.SaveChangesAsync();
            }
        }

        public async Task UpdateCard(Card card)
        {
            _dbContext.Cards.Update(card);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Vote(Card card)
        {
            card.VoteCount++;
            _dbContext.Cards.Update(card);
            await _dbContext.SaveChangesAsync();
        }
    }

    public interface ICardService
    {
        Task<Card> AddCard(Card card);
        List<Card> GetCardsByCategory(int categoryId);
        List<Card> GetCardsInGroup(int groupId);
        Task Delete(Card card);
        Task UpdateCard(Card card);
        Task Vote(Card card);
    }
}