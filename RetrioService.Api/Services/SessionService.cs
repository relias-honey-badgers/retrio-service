﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RetrioService.Api.Entities;

namespace RetrioService.Api.Services
{
    public interface ISessionService
    {
        Task<Session> GetSessionById(Guid guid);
        Task<Session> CreateSession(Session session);
        Task UpdateSession(Session session);
    }

    public class SessionService : ISessionService
    {
        private readonly RetrioContext _db;

        public SessionService(RetrioContext db)
        {
            _db = db;
        }

        public async Task<Session> CreateSession(Session session)
        {
            _db.Sessions.Add(session);
            await _db.SaveChangesAsync();
            return session;
        }

        public async Task UpdateSession(Session session)
        {
            _db.Sessions.Update(session);
            await _db.SaveChangesAsync();
        }

        public Task<Session> GetSessionById(Guid guid)
        {
            return _db.Sessions.FirstOrDefaultAsync(s => s.SessionId == guid);
        }
    }
}