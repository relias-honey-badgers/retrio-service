﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RetrioService.Api.Entities;
using RetrioService.Api.Services;

namespace RetrioService.Api.Controllers
{
    [Route("api/users")]
    public class UsersController : Controller
    {
        private readonly IUserService _service;

        public UsersController(IUserService service)
        {
            _service = service;
        }

        [HttpGet("{guid}")]
        public Task<User> GetUser(Guid guid)
        {
            return _service.GetUser(guid);
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser([FromBody] User user)
        {
            await _service.CreateUser(user);
            return Created($"{Request.Path}/{user.UserId}", user);
        }
    }
}