﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RetrioService.Api.Entities;
using RetrioService.Api.Services;

namespace RetrioService.Api.Controllers
{
    [Route("api/sessions")]
    public class SessionsController : Controller
    {
        private readonly ISessionService _service;

        public SessionsController(ISessionService service)
        {
            _service = service;
        }

        [HttpGet("{guid}")]
        public Task<Session> GetSession(Guid guid)
        {
            return _service.GetSessionById(guid);
        }

        [HttpPost]
        public async Task<IActionResult> CreateSession([FromBody]Session session)
        {
            var sessionWithId = await _service.CreateSession(session);
            return Created($"{Request.Path}/{sessionWithId.SessionId}", sessionWithId);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateSession([FromBody] Session session)
        {
            await _service.UpdateSession(session);
            return NoContent();
        }
    }
}