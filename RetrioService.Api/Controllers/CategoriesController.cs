﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RetrioService.Api.Entities;
using RetrioService.Api.Services;

namespace RetrioService.Api.Controllers
{
    [Route("api/categories")]
    public class CategoriesController : Controller
    {
        private readonly ICategoryService _service;

        public CategoriesController(ICategoryService service)
        {
            _service = service;
        }

        [HttpGet("{guid}")]
        public List<Category> GetBySessionId(Guid guid)
        {
            return _service.GetCategoriesBySession(guid);
        }

        [HttpGet("/api/sessions/{guid}/categories")]
        public List<Category> GetBySessionId2(Guid guid)
        {
            return GetBySessionId(guid);
        }

        [HttpPost]
        public async Task<IActionResult> CreateCategory([FromBody] Category category)
        {
            await _service.CreateCategory(category);
            return Created($"{Request.Path}/{category.CategoryId}", category);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateCategory([FromBody] Category category)
        {
            await _service.UpdateCategory(category);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategory(int id)
        {
            await _service.DeleteCategory(id);
            return NoContent();
        }
    }

    
}