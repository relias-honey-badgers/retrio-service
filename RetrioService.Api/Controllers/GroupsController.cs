﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RetrioService.Api.Entities;
using RetrioService.Api.Services;

namespace RetrioService.Api.Controllers
{
    [Route("api/groups")]
    public class GroupsController : Controller
    {
        private readonly IGroupService _service;

        public GroupsController(IGroupService service)
        {
            _service = service;
        }

        [HttpGet("{id}")]
        public Task<List<Group>> GetGroupsByCategory(int id)
        {
            return _service.GetByCategoryId(id);
        }
    }

   
}