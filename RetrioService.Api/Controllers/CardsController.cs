﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using RetrioService.Api.Entities;
using RetrioService.Api.Services;

namespace RetrioService.Api.Controllers
{
    [Route("api/cards")]
    public class CardsController : Controller
    {
        private readonly ICardService _service;

        public CardsController(ICardService service)
        {
            _service = service;
        }

        [HttpGet("{id}")]
        public List<Card> GetCardsByCategory(int id)
        {
            return _service.GetCardsByCategory(id);
        }
    }
}