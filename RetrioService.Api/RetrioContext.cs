﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using RetrioService.Api.Entities;

namespace RetrioService.Api
{
    public class RetrioContext : DbContext
    {
		public DbSet<Session> Sessions { get; set; }
		public DbSet<Category> Categories { get; set; }
		public DbSet<Card> Cards { get; set; }
		public DbSet<Group> Groups { get; set; }
        public DbSet<User> Users { get; set; }
        public RetrioContext(DbContextOptions options) : base(options)
        {
        }
    }

    public class RetrioContextFactory : IDesignTimeDbContextFactory<RetrioContext>
    {
        public RetrioContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder();
            builder.UseSqlServer("Server=tcp:retrio.database.windows.net,1433;Initial Catalog=retrio;Persist Security Info=False;User ID=retrioadmin;Password=s##3h@!SUdLEY2lwB9PI;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            return new RetrioContext(builder.Options);
        }
    }
}

