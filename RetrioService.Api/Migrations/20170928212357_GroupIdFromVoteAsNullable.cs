﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace RetrioService.Api.Migrations
{
    public partial class GroupIdFromVoteAsNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "GroupId",
                table: "Votes",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Votes_GroupId",
                table: "Votes",
                column: "GroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_Votes_Groups_GroupId",
                table: "Votes",
                column: "GroupId",
                principalTable: "Groups",
                principalColumn: "GroupId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Votes_Groups_GroupId",
                table: "Votes");

            migrationBuilder.DropIndex(
                name: "IX_Votes_GroupId",
                table: "Votes");

            migrationBuilder.DropColumn(
                name: "GroupId",
                table: "Votes");
        }
    }
}
