﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace RetrioService.Api.Migrations
{
    public partial class CardFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Votes_CardId",
                table: "Votes",
                column: "CardId");

            migrationBuilder.AddForeignKey(
                name: "FK_Votes_Cards_CardId",
                table: "Votes",
                column: "CardId",
                principalTable: "Cards",
                principalColumn: "CardId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Votes_Cards_CardId",
                table: "Votes");

            migrationBuilder.DropIndex(
                name: "IX_Votes_CardId",
                table: "Votes");
        }
    }
}
