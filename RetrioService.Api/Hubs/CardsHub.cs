﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using RetrioService.Api.Entities;
using RetrioService.Api.Services;

namespace RetrioService.Api.Hubs
{
    public class CardsHub : Hub
    {
        private readonly ICardService _cardService;

        public CardsHub(ICardService cardService)
        {
            _cardService = cardService;
        }

        public async Task Add(Card card)
        {
            await _cardService.AddCard(card);
            await Clients.All.InvokeAsync("Add", card);
        }

        public async Task Delete(Card card)
        {
            await _cardService.Delete(card);
            await UpdateOtherClients(card, "Delete");
        }

        public async Task Update(Card card)
        {
            await _cardService.UpdateCard(card);
            await UpdateOtherClients(card, "Update");
        }

        public async Task Vote(Card card)
        {
            await _cardService.Vote(card);
            await Clients.All.InvokeAsync("Update", card);
        }

        private Task UpdateOtherClients(Card card, string action)
        {
            return Clients.AllExcept(new[] {Context.ConnectionId}).InvokeAsync(action, card);
        }
    }
}