﻿using System;
namespace RetrioService.Api.Entities
{
    public class Session
    {
        public Guid SessionId { get; set; } = Guid.NewGuid();
        public string Title { get; set; }
    }
}
