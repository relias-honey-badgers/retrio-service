﻿namespace RetrioService.Api.Entities
{
    public class Group
    {
        public int GroupId { get; set; }
        public string Caption { get; set; }
    }
}
