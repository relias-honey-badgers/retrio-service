﻿using System;

namespace RetrioService.Api.Entities
{
    public class Card
    {
        public int CardId { get; set; }
        public Category Category { get; set; }
        public int CategoryId { get; set; }
        public Guid UserId { get; set; }
        public string Text { get; set; }
        public int? GroupId { get; set; }
        public Group Group { get; set; }
        public int VoteCount { get; set; }
    }
}
