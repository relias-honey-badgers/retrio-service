﻿using System;
namespace RetrioService.Api.Entities
{
    public class Category
    {
        public int CategoryId { get; set; }
        public Guid SessionId { get; set; }
        public Session Session { get; set; }
        public string Name { get; set; }
        public int Ordinal { get; set; }
    }
}
