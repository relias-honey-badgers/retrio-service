﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using RetrioService.Api.Hubs;
using RetrioService.Api.Services;
using Swashbuckle.AspNetCore.Swagger;

namespace RetrioService.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddSignalR(o => o.JsonSerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver());
            services.AddMvc().AddJsonOptions(op => {
                op.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
				op.SerializerSettings.NullValueHandling = NullValueHandling.Include;
				op.SerializerSettings.Converters.Add(new StringEnumConverter());
				op.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });

            services.AddDbContext<RetrioContext>(o =>
                o.UseSqlServer(
                    "Server=tcp:retrio.database.windows.net,1433;Initial Catalog=retrio;Persist Security Info=False;User ID=retrioadmin;Password=s##3h@!SUdLEY2lwB9PI;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"));

            services.AddScoped<ISessionService, SessionService>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<ICardService, CardService>();
            services.AddScoped<IGroupService, GroupService>();
            services.AddScoped<IUserService, UserService>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info {Title = "Retrio", Version = "v1"});
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(policy =>
            {
                policy.AllowAnyOrigin();
                policy.AllowAnyHeader();
                policy.AllowAnyMethod();
            });

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseSignalR(routes =>
            {
                routes.MapHub<CardsHub>("cards");
            });

            app.UseMvc();
        }
    }
}
